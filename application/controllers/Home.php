<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    public function index()
    {
        $data['title'] = "Halaman Data Pelanggan";
        $data['ci'] = $this->M_mahasiswa->SemuaData();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/topbar');
        $this->load->view('templates/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah_data()
    {
        $data['title'] = "Halaman Tambah Pelanggan";
        $data['ci'] = $this->M_mahasiswa->SemuaData();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/topbar');
        $this->load->view('home/tambah_data', $data);
        $this->load->view('templates/footer');
    }

    public function proses_tambah_data()
    {
        $this->M_mahasiswa->proses_tambah_data();
        $this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
                                    Data Berhasil Ditambahkan !!
                                    </div>'); // ini digunakan untuk membuat pesan/alert bahwa data berhasil ditambahkan
        redirect('Home');
    }

    public function hapus_data($id)
    {
        $this->M_mahasiswa->hapus_data($id);
        redirect('Home');
    }

    public function edit_data($id)
    {
        $data['title'] = "Edit Data";
        $data['ci'] = $this->M_mahasiswa->ambil_id_mahasiswa($id);
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/topbar');
        $this->load->view('home/edit_data', $data);
        $this->load->view('templates/footer');
    }

    public function proses_edit_data($id = 0)
    {
        $this->M_mahasiswa->proses_edit_data($id);
        redirect('Home');
    }
}
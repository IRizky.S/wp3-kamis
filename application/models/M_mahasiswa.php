<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_mahasiswa extends CI_Model

    {
        public function SemuaData()
        {
            return $this->db->get('ci')->result_array();
        }

        public function proses_tambah_data()
        {
            $data = [

                "nama" => $this->input->post('nama'),
                "nim" => $this->input->post('nim'),
                "alamat" => $this->input->post('alamat'),

            ];

            $this->db->insert('ci', $data);
        }

        public function hapus_data($id)
        {
            $this->db->where('id', $id);
            $this->db->delete('ci');
        }

        public function ambil_id_mahasiswa($id)
        {
            return $this->db->get_where('ci', ['id' => $id])->row_array();

        }

        public function proses_edit_data($id)
        {
            $data = [
                "nama" => $this->input->post('nama'),
                "nim" => $this->input->post('nim'),
                "alamat" => $this->input->post('alamat'),
            ];

            $this->db->where('id', $this->input->post('id'));
            $this->db->update('ci' , $data);
        }
    }